﻿using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public interface IDataContextFactory
    {
        public DbContext CreateContext();
    }
}