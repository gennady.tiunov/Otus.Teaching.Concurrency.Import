﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.Concurrency.Import.Core.Entities;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        public DataContext(DbContextOptions<DataContext> dbContextOptions) : base(dbContextOptions)
        {
            //Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.ApplyConfiguration(new CustomerConfiguration());
        }

        public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
        {
            public void Configure(EntityTypeBuilder<Customer> builder)
            {
                builder.ToTable("Customer").HasKey(p => p.Id);
                builder.Property(p => p.Id).ValueGeneratedNever();
                builder.Property(p => p.FullName).IsRequired().HasMaxLength(100);
                builder.Property(p => p.Email).IsRequired().HasMaxLength(50);
                builder.Property(p => p.Phone).IsRequired().HasMaxLength(15);
            }
        }
    }
}
