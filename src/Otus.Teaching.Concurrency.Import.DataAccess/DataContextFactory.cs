﻿using Microsoft.EntityFrameworkCore;
using Npgsql.EntityFrameworkCore.PostgreSQL.Infrastructure;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContextFactory : IDataContextFactory
    {
        private readonly string _connectionString;
        
        public DataContextFactory(string connectionString)
        {
            _connectionString = connectionString; 
        }

        public DbContext CreateContext()
        {
            var contextOptionsBuilder = new DbContextOptionsBuilder<DataContext>();
            contextOptionsBuilder.UseNpgsql(_connectionString);
            
            var postgresOptionsBuilder = new NpgsqlDbContextOptionsBuilder(contextOptionsBuilder);
            postgresOptionsBuilder.EnableRetryOnFailure();
                        
            return new DataContext(contextOptionsBuilder.Options);
        }
    }
}