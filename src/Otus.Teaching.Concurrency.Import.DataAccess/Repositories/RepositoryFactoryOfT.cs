using Otus.Teaching.Concurrency.Import.Core.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class RepositoryFactory<T> : IRepositoryFactory<T> where T : class, new()
    {
        private readonly IDataContextFactory _dataContextFactory;

        public RepositoryFactory(IDataContextFactory dataContextFactory)
        {
            _dataContextFactory = dataContextFactory;
        }

        public IRepository<T> CreateRepository()
        {
            return new Repository<T>(_dataContextFactory);
        }
    }
}