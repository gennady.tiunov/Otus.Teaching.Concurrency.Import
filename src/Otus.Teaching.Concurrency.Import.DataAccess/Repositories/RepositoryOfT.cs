using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class Repository<T> : IRepository<T> where T : class, new()
    {
        private readonly DbContext _dataContext;

        public Repository(
            IDataContextFactory dataContextFactory)
        {
            _dataContext = dataContextFactory.CreateContext();
        }

        public async Task AddAsync(T entity)
        {
            await _dataContext.Set<T>().AddAsync(entity);
            await _dataContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            _dataContext?.Dispose();
        }
    }
}