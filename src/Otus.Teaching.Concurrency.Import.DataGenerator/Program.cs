﻿using System;
using CommandLine;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Generators;
using Otus.Teaching.Concurrency.Import.DataGenerator.CommandLine;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generation;
using Otus.Teaching.Concurrency.Import.Serialization;

var hostBuilder = Host
    .CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        services.AddSingleton(typeof(IEntitySerializerOfT<>), typeof(ReflectionBasedCsvEntitySerializer<>));
        services.AddSingleton<IEntityGenerator<Customer>, CustomerGenerator>();
        services.AddSingleton<IDataFileGenerator, CustomerFileGenerator>();
        services.AddSingleton<Program>();
    });

var host = hostBuilder.Build();
host.Services.GetRequiredService<Program>().Run(args);

public partial class Program
{
    private readonly IDataFileGenerator _dataFileGenerator;
    
    public Program(
        IDataFileGenerator dataFileGenerator)
    {
        _dataFileGenerator = dataFileGenerator;
    }

    private void Run(string[] args)
    {
        Parser.Default.ParseArguments<Parameters>(args)
            .WithParsed(parameters =>
            {
                if (!ParametersValidator.Validate(parameters, out var errorMessage))
                {
                    Console.WriteLine(errorMessage);
                    Environment.Exit(1);
                }
                
                Console.WriteLine("Generating data file.");
                
                _dataFileGenerator.Generate(parameters.FilePath, parameters.EntityCount);
                
                Console.WriteLine($"{parameters.EntityCount} entities saved to '{parameters.FilePath}'.");
            });
    }
}