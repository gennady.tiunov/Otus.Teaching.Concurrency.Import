﻿using CommandLine;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.CommandLine
{
    public class Parameters
    {
        [Option(
            "file-path",
            Required = true,
            HelpText = "Path of file to generate entities to.")]
        public string FilePath { get; set; }

        [Option(
            "entity-count",
            Required = true,
            HelpText = "Number of entities to generate.")]
        public int EntityCount { get; set; }
    }
}
