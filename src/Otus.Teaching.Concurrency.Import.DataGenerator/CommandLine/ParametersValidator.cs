﻿using System;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.CommandLine
{
    public class ParametersValidator
    {
        public static bool Validate(Parameters parameters, out string errorMessage)
        {
            const string message = "Validating entered parameters";
            Console.WriteLine(message);
            
            errorMessage = string.Empty;
            
            if (parameters.EntityCount <= 0)
            {
                errorMessage = "Please, specify number of entities correctly";
                return false;
            }

            return true;
        }
    }
}