using System.IO;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Generators;
using Otus.Teaching.Concurrency.Import.Serialization;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generation
{
    public class CustomerFileGenerator : IDataFileGenerator
    {
        private readonly IEntitySerializerOfT<Customer> _customerSerializer;
        private readonly IEntityGenerator<Customer> _customerGenerator;
        
        public CustomerFileGenerator(
            IEntitySerializerOfT<Customer> customerSerializer,
            IEntityGenerator<Customer> customerGenerator)
        {
            _customerSerializer = customerSerializer;
            _customerGenerator = customerGenerator;
        }
        
        public void Generate(string filePath, int _entityCount)
        {
            var customers = _customerGenerator.Generate(_entityCount);

            var fileContent = _customerSerializer.Serialize(customers);

            File.WriteAllText(filePath, fileContent);
        }
    }
}