using System.Collections.Generic;
using Bogus;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Generators;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generation
{
    public class CustomerGenerator : IEntityGenerator<Customer>
    {
        public IEnumerable<Customer> Generate(int customerCount)
        {
            var customers = new List<Customer>();
            var customersFaker = CreateFaker();

            foreach (var customer in customersFaker.GenerateForever())
            {
                customers.Add(customer);

                if (customerCount == customer.Id)
                {
                    return customers;
                }
            }

            return customers;
        }

        private static Faker<Customer> CreateFaker()
        {
            var id = 1;

            var customersFaker = new Faker<Customer>()
                .CustomInstantiator(_ => new Customer()
                {
                    Id = id++
                })
                .RuleFor(u => u.FullName, (f, _) => f.Name.FullName())
                .RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.FullName))
                .RuleFor(u => u.Phone, (f, _) => f.Phone.PhoneNumber("1-###-###-####"));

            return customersFaker;
        }
    }
}