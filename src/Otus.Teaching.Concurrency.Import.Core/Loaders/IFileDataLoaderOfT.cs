﻿using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public interface IFileDataLoader<T> where T: class, new()
    {
        IEnumerable<T> Load(string filePath);
    }
}