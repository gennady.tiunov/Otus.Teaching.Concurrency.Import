﻿using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public interface IDbDataImporter<T> where T: class, new()
    {
        void Import(
            IEnumerable<T> entities,
            int degreeOfParallelism);
    }
}