namespace Otus.Teaching.Concurrency.Import.Core.Generators
{
    public interface IDataFileGenerator
    {
        void Generate(string filePath, int entityCount);
    }
}