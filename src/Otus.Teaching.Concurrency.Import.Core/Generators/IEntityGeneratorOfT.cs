using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.Core.Generators
{
    public interface IEntityGenerator<T> where T: class, new()
    {
        IEnumerable<T> Generate(int entityCount);
    }
}