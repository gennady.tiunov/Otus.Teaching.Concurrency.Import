namespace Otus.Teaching.Concurrency.Import.Core.Repositories
{
    public interface IRepositoryFactory<T> where T : class, new()
    {
        IRepository<T> CreateRepository();
    }
}