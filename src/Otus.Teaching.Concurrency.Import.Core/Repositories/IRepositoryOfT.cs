using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core.Repositories
{
    public interface IRepository<T> : IDisposable  where T : class, new()
    {
        Task AddAsync(T entity);
    }
}