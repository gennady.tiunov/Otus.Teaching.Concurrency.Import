﻿using System;
using System.IO;
using CommandLine;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.CommandLine;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using Otus.Teaching.Concurrency.Import.Serialization;
using Otus.Teaching.PromoCodeFactory.DataAccess;

var hostBuilder = Host
    .CreateDefaultBuilder(args)
    .ConfigureAppConfiguration((h,c)=>
    {
        c.SetBasePath(Directory.GetCurrentDirectory());
        c.AddJsonFile("appsettings.json", true, true);
    })
    .ConfigureServices((h,services) =>
    {
        /*services.AddDbContext<DataContext>(x =>
        {
            x.UseNpgsql(h.Configuration["CustomersDB"]);
        }, ServiceLifetime.Singleton);*/
        
        services.AddSingleton<IRangeSplitter, RangeSplitter>();
        
        services.AddSingleton(typeof(IEntitySerializerOfT<>), typeof(ReflectionBasedCsvEntitySerializer<>));
        services.AddSingleton(typeof(IRepositoryFactory<>), typeof(RepositoryFactory<>));
        services.AddSingleton<IDataContextFactory>(new DataContextFactory(h.Configuration.GetConnectionString("CustomersDB")));
        services.AddSingleton(typeof(IFileDataLoader<>), typeof(FileDataLoader<>));
        services.AddSingleton(typeof(IDbDataImporter<>), typeof(ConcurrentDbDataImporter<>));
        
        services.AddSingleton<Program>();
    });

var host = hostBuilder.Build();
host.Services.GetRequiredService<Program>().Run(args);

public partial class Program
{
    private readonly IFileDataLoader<Customer> _customerFileDataLoader;
    private readonly IDbDataImporter<Customer> _customerDbImporter;

    public Program(
        IFileDataLoader<Customer> customerFileDataLoader,
        IDbDataImporter<Customer> customerDbImporter)
    {
        _customerFileDataLoader = customerFileDataLoader;
        _customerDbImporter = customerDbImporter;
    }
    
    private void Run(string[] args)
    {
        Parser.Default.ParseArguments<Parameters>(args)
            .WithParsed(parameters =>
            {
                if (!ParametersValidator.Validate(parameters, out var errorMessage))
                {
                    Console.WriteLine(errorMessage);
                    Environment.Exit(1);
                }
                
                var customers = _customerFileDataLoader.Load(parameters.FilePath);
        
                _customerDbImporter.Import(customers, parameters.DegreeOfParallelism);
            });
    }
}