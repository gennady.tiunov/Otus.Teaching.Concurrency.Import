﻿namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class RangeSplitter : IRangeSplitter
    {
        public (int lowerIndex, int upperIndex) GetSubRangeIndices(
            int rangeLength,
            int chunkCount,
            int chunkIndex)
        {
            var chunkSize = rangeLength / chunkCount;
            
            var lowerIndex = chunkIndex  * chunkSize;
            
            var upperIndex = chunkIndex < chunkCount - 1
                ? (chunkIndex + 1) * chunkSize - 1
                : ((chunkIndex + 1 ) * chunkSize - 1) + rangeLength % chunkCount;
            
            return (lowerIndex, upperIndex);
        }
    }
}