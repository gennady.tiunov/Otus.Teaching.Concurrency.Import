using System;
using System.Collections.Generic;
using System.IO;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Serialization;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class FileDataLoader<T>
        : IFileDataLoader<T> where T : class, new()
    {
        private readonly IEntitySerializerOfT<T> _entitySerializer; 
        
        public FileDataLoader(
            IEntitySerializerOfT<T> entitySerializer)
        {
            _entitySerializer = entitySerializer;
        }

        public IEnumerable<T> Load(string filePath)
        {
            Console.WriteLine($"Starting to load entities of type '{typeof(T).Name}' from file '{filePath}'.");

            var fileContent = File.ReadAllText(filePath);
            
            var entities = _entitySerializer.Deserialize(fileContent);
            
            Console.WriteLine("Entities loaded from file.");
            
            return entities;
        }
    }
}