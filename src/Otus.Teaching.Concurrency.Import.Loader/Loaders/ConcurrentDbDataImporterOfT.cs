using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Repositories;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class ConcurrentDbDataImporter<T>
        : IDbDataImporter<T> where T : class, new()
    {
        private List<T> _entities;
        private readonly IRepositoryFactory<T> _repositoryFactory;
        private readonly IRangeSplitter _rangeSplitter;
        
        private class ChunkImportContext
        {
            public int LowerIndex { get; set; }
            public int UpperIndex { get; set; }
            public ManualResetEvent DoneEvent { get; set; }
        }
        
        public ConcurrentDbDataImporter(
            IRepositoryFactory<T> repositoryFactory,
            IRangeSplitter rangeSplitter)
        {
            _repositoryFactory = repositoryFactory;
            _rangeSplitter = rangeSplitter;
        }

        public void Import(
            IEnumerable<T> entities,
            int degreeOfParallelism)
        {
            _entities = entities.ToList();
            
            Console.WriteLine($"Starting to import {_entities.Count()} entities of type '{typeof(T).Name}' to DB.");

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var doneEvents = new ManualResetEvent[degreeOfParallelism];
                
            for (var chunkIndex = 0; chunkIndex < degreeOfParallelism; chunkIndex++)
            {
                var (lowerIndex, upperIndex) = _rangeSplitter.GetSubRangeIndices(
                    entities.Count(),
                     degreeOfParallelism,
                    chunkIndex);

                var chunkContext = new ChunkImportContext
                {
                    LowerIndex = lowerIndex,
                    UpperIndex = upperIndex,
                    DoneEvent = doneEvents[chunkIndex] = new ManualResetEvent(false) 
                };
                
                ThreadPool.QueueUserWorkItem(ImportChunk, chunkContext);
            }

            WaitHandle.WaitAll(doneEvents);
            
            stopwatch.Stop();
            
            Console.WriteLine($"{_entities.Count()} entities imported to DB by {degreeOfParallelism} thread(s) in {stopwatch.ElapsedMilliseconds} milliseconds.");
        }
        
        private void ImportChunk(object threadContext)
        {
            var context = (ChunkImportContext) threadContext;
            
            var chunkEntities = _entities.GetRange(context.LowerIndex, context.UpperIndex - context.LowerIndex + 1);
            
            //Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId}: Starting to import {chunkEntities.Count()} entities to DB.");

            using var repository = _repositoryFactory.CreateRepository();
            
            foreach (var entity in chunkEntities)
            {
                repository.AddAsync(entity).Wait();
            }

            context.DoneEvent.Set();
            
            //Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId}: {chunkEntities.Count()} entities imported to DB.");
        }
    }
}