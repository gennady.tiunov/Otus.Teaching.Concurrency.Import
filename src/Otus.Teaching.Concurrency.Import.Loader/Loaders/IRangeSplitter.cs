﻿namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public interface IRangeSplitter
    {
        (int lowerIndex, int upperIndex) GetSubRangeIndices(
            int rangeLength,
            int chunkCount,
            int chunkIndex);
    }
}