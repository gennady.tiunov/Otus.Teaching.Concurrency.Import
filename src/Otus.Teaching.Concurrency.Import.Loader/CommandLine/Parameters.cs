﻿using CommandLine;

namespace Otus.Teaching.Concurrency.Import.Loader.CommandLine
{
    public class Parameters
    {
        [Option(
            "file-path",
            Required = true,
            HelpText = "Path of file to generate entities to.")]
        public string FilePath { get; set; }
        
        [Option(
            "thread-count",
            Required = true,
            HelpText = "Number of threads to use for importing entities to DB.")]
        public int DegreeOfParallelism { get; set; }
    }
}