﻿using System;

namespace Otus.Teaching.Concurrency.Import.Loader.CommandLine
{
    public class ParametersValidator
    {
        public static bool Validate(Parameters parameters, out string errorMessage)
        {
            const string message = "Validating entered parameters";
            Console.WriteLine(message);
            
            errorMessage = string.Empty;
            
            if (parameters.DegreeOfParallelism <= 0)
            {
                errorMessage = "Please, specify number of threads correctly";
                return false;
            }

            return true;
        }
    }
}