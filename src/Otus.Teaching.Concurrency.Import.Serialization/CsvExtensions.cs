﻿using Otus.Teaching.Concurrency.Import.Serialization;

namespace Otus.Teaching.Concurrency.Import.Csv
{
    public static class CsvExtensions
    {
        public static string? EscapeQuotes(this string? value)
        {
            return value?.Replace("\"", "\"\"");
        }

        public static string NormalizeQuotes(this string value)
        {
            return value.Replace("\"\"", "\"");
        }

        public static string? EscapeDelimiter(this string? value)
        {
            return value?.Replace(Constants.CsvDelimiter, $"{Constants.CsvDelimiter}{Constants.CsvDelimiter}");
        }

        public static string NormalizeDelimiter(this string value)
        {
            return value.Replace($"{Constants.CsvDelimiter}{Constants.CsvDelimiter}", Constants.CsvDelimiter);
        }

        public static string Quote(this string? value)
        {
            return $"\"{value}\"";
        }
    }
}
