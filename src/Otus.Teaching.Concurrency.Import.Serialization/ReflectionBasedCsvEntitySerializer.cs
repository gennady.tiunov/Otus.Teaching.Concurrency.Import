﻿using System.ComponentModel;
using System.Reflection;
using System.Text;
using Otus.Teaching.Concurrency.Import.Csv;

namespace Otus.Teaching.Concurrency.Import.Serialization;

public class ReflectionBasedCsvEntitySerializer<T> : IEntitySerializerOfT<T>
    where T : class, new()
{
    public string Serialize(IEnumerable<T> entities)
    {
        if (entities == null)
        {
            throw new ArgumentNullException(nameof(entities));
        }

        var entityType = typeof(T);

        var properties = entityType.GetProperties(BindingFlags.Instance | BindingFlags.Public).ToArray();
        if (properties.Length == 0)
        {
            throw new SerializationException($"Could not serialize entity '{entityType.Name}' as it has no public properties");
        }

        var csv = new StringBuilder();

        foreach (var property in properties)
        {
            csv.Append(property.Name);
            csv.Append(Constants.CsvDelimiter);
        }

        csv.Replace(Constants.CsvDelimiter, Environment.NewLine, csv.Length - 1, 1);

        foreach (var entity in entities)
        {
            foreach (var property in properties)
            {
                var value = property.GetValue(entity)?.ToString();
                    
                csv.Append(value.EscapeDelimiter().EscapeQuotes().Quote());
                csv.Append(Constants.CsvDelimiter);
            }

            csv.Replace(Constants.CsvDelimiter, Environment.NewLine, csv.Length - 1, 1);
        }

        return csv.ToString();
    }

    public IEnumerable<T> Deserialize(string serializedEntities)
    {
        var columnNames = AssertValidCsvData(serializedEntities);

        var result = new List<T>();

        using var reader = new StringReader(serializedEntities);

        reader.ReadLine();

        var valueLine = reader.ReadLine();
        while (valueLine != null)
        {
            result.Add(SerializeEntity(valueLine, columnNames));
            valueLine = reader.ReadLine();
        }

        return result;
    }

    private string[] AssertValidCsvData(string serializedEntities)
    {
        if (string.IsNullOrEmpty(serializedEntities))
        {
            throw new ArgumentNullException(nameof(serializedEntities));
        }

        using var reader = new StringReader(serializedEntities);

        var headerLine = reader.ReadLine();
        if (string.IsNullOrEmpty(headerLine))
        {
            throw new SerializationException("Header line in document is empty");
        }

        var columnNames = headerLine.Split(Constants.CsvDelimiter, StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
        if (columnNames.Length == 0)
        {
            throw new SerializationException("Header line has no column names specified");
        }

        var valueLine = reader.ReadLine();
        if (string.IsNullOrEmpty(valueLine))
        {
            throw new SerializationException("Document contains no entities to deserialize");
        }

        var values = valueLine.Split($"\"{Constants.CsvDelimiter}\"", StringSplitOptions.TrimEntries);
        if (values.Length == 0)
        {
            throw new SerializationException("Entity line has no values specified");
        }

        if (columnNames.Length != values.Length)
        {
            throw new SerializationException($"Number of columns ({columnNames.Length}) does not coincide number of entity properties / fields ({values.Length})");
        }

        return columnNames;
    }
        
    private T SerializeEntity(string entityLine, string[] columnNames)
    {
        var result = new T();
            
        var values = entityLine.Split($"\"{Constants.CsvDelimiter}\"", StringSplitOptions.TrimEntries);

        if (values.Length == 0)
        {
            throw new SerializationException("Entity line has no values specified");
        }

        if (columnNames.Length != values.Length)
        {
            throw new SerializationException(
                $"Number of columns ({columnNames.Length}) does not coincide number of entity properties / fields ({values.Length})");
        }
            
        for (var i = 0; i < columnNames.Length; i++)
        {
            var entityType = typeof(T);

            try
            {
                var value = values[i]?.Trim('"').NormalizeQuotes().NormalizeDelimiter();

                var property = entityType.GetProperty(columnNames[i], BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public);
                if (property != null)
                {
                    if (value != null)
                    {
                        var propertyType = property.PropertyType;
                        var typeConverter = TypeDescriptor.GetConverter(propertyType);
                        property.SetValue(result, typeConverter.ConvertFrom(value));
                    }
                    continue;
                }
                var field = entityType.GetField(columnNames[i], BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                if (field != null)
                {
                    if (value != null)
                    {
                        var fieldType = field.FieldType;
                        var typeConverter = TypeDescriptor.GetConverter(fieldType);
                        field.SetValue(result, typeConverter.ConvertFrom(value));
                    }
                    continue;
                }
            }
            catch (Exception exception)
            {
                throw new SerializationException($"Error occurred while setting value '{values[i]}' to property / field '{columnNames[i]}' on entity '{entityType.Name}': {exception.Message}");
            }

            throw new SerializationException($"Neither property nor field '{columnNames[i]}' found on entity '{entityType.Name}'");
        }

        return result;
    }
}