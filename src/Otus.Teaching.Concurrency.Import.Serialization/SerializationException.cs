﻿namespace Otus.Teaching.Concurrency.Import.Serialization;

public class SerializationException : InvalidOperationException
{
    public SerializationException(string message) : base(message) { }
}