﻿namespace Otus.Teaching.Concurrency.Import.Serialization;

public interface IEntitySerializerOfT<T> where T : class, new()
{
    string Serialize(IEnumerable<T> entity);

    IEnumerable<T> Deserialize(string serializedEntity);
}